﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using HomeKeeper.Models;
using HomeKeeper.Utilities;
using HomeKeeper.Utilities.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;
using Windows.UI.Xaml;

namespace HomeKeeper.ViewModels
{
    public class CreateBillViewModel : ViewModelBase
    {
        private Payment _payment = new Payment();
        public Payment Payment
        {
            get { return _payment; }
            set
            {
                if (_payment != value)
                {   
                    _payment = value;
                    RaisePropertyChanged();
                }
            }
        }

        private List<HouseMateResult> _houseMates = new List<HouseMateResult>();
        public List<HouseMateResult> HouseMates
        {
            get { return _houseMates; }
            set
            {
                if (_houseMates != value)
                {
                    _houseMates = value;
                    RaisePropertyChanged();
                }
            }
        }

        private HouseMateResult _selectedHouseMate;

        public HouseMateResult SelectedHouseMate
        {
            get { return _selectedHouseMate; }
            set
            {
                if (_selectedHouseMate != value)
                {
                    _selectedHouseMate = value;
                    RaisePropertyChanged();
                }
            }
        }

        private double _amount;
        public double Amount
        {
            get { return _amount; }
            set
            {
                if (_amount != value)
                {
                    _amount = value;
                    RaisePropertyChanged();
                }
            }
        }

        private List<String> _categoryValues = new List<string>();
        public List<String> CategoryValues
        {
            get { return _categoryValues; }
            set
            {
                if (_categoryValues != value)
                {
                    _categoryValues = value;
                    RaisePropertyChanged();
                }
            }
        }

        private String _selectedCategory;
        public String SelectedCategory
        {
            get { return _selectedCategory; }
            set
            {
                if (_selectedCategory != value)
                {
                    _selectedCategory = value;
                    RaisePropertyChanged();
                }
            }
        }

        public RelayCommand HomeCommand { get; set; }
        public RelayCommand ProfileCommand { get; set; }
        public RelayCommand LogoutCommand { get; set; }
        public RelayCommand CreateBillCommand { get; set; }

        public CreateBillViewModel()
        {
            GetHouseMates();            
            SetupNavigation();
            SetupCommands();
            SetupComboBoxValues();      
        }

        public void SetupNavigation()
        {
            HomeCommand = new RelayCommand(NavigationUtility.NavigateToHome);
            ProfileCommand = new RelayCommand(NavigationUtility.NavigateToProfile);
            LogoutCommand = new RelayCommand(NavigationUtility.NavigateToLogin);
        }

        public void SetupCommands()
        {
            CreateBillCommand = new RelayCommand(CreateBill);
        }

        public void SetupComboBoxValues()
        {
            CategoryValues.Add(DialogMessagesErrorsAndConstants.CategoryRent);
            CategoryValues.Add(DialogMessagesErrorsAndConstants.CategoryElectricity);
            CategoryValues.Add(DialogMessagesErrorsAndConstants.CategoryHeating);
            CategoryValues.Add(DialogMessagesErrorsAndConstants.CategoryFood);
            CategoryValues.Add(DialogMessagesErrorsAndConstants.CategoryClothing);
            CategoryValues.Add(DialogMessagesErrorsAndConstants.CategoryMisc);            
        }

        private async void CreateBill()
        {
            if (NetworkInformation.GetInternetConnectionProfile() != null)
            {
                try
                {
                    if (ValidateInput())
                    {
                        if (SelectedHouseMate.FirstName == DialogMessagesErrorsAndConstants.PayAllHousemates)
                        {
                            List<Payment> _payAllList = GetPaymentForAllHousemates();
                            foreach (Payment payment in _payAllList)
                            {
                                await App.PaymentTable.InsertAsync(payment);
                            }
                            _payAllList = null;
                            DialogUtility.ShowMobileMessageDialog(DialogMessagesErrorsAndConstants.BillSentToAll);
                            NavigationUtility.NavigateToMyFinances();                       
                        }
                        else
                        {
                            GetPayment();
                            await App.PaymentTable.InsertAsync(Payment);
                            Payment = null;
                            DialogUtility.ShowMobileMessageDialog(DialogMessagesErrorsAndConstants.BillSent);
                            NavigationUtility.NavigateToMyFinances();                      
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                    DialogUtility.ShowMessage(DialogMessagesErrorsAndConstants.ErrorOccured);
                }
            }
            else
            {
                DialogUtility.NoNetworkConnection();
            }
        }

        private async void GetHouseMates()
        {
            if (NetworkInformation.GetInternetConnectionProfile() != null)
            {
                try
                {
                    List<String> namesList = new List<string>();

                    JArray result = await App.MobileService
                            .InvokeApiAsync<JArray>(DialogMessagesErrorsAndConstants.HouseMateController,
                                System.Net.Http.HttpMethod.Get, new Dictionary<string, string>
                                                    {
                                                        {"leaseId", ApplicationResourcesUtility.GetMyLease().Id}
                                                    });

                    List<HouseMateResult> houseMates = result.ToObject<List<HouseMateResult>>()
                        .Where(housemate => housemate.Id != ApplicationResourcesUtility.GetLoggedOnUser().Id).ToList();

                    if (houseMates.Count > 0)
                    {               
                        HouseMates = houseMates;
                    }

                    if (houseMates.Count > 1)
                    {
                        houseMates.Add(new HouseMateResult { FirstName = DialogMessagesErrorsAndConstants.PayAllHousemates });
                        HouseMates = houseMates;
                    }

                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                    DialogUtility.ShowMessage(DialogMessagesErrorsAndConstants.ErrorOccured);
                }
            }
            else
            {
                DialogUtility.NoNetworkConnection();
            }
        }
        public string GetCategory()
        {
            if (SelectedCategory.Equals("0"))
            {
                return DialogMessagesErrorsAndConstants.CategoryRent;
            }
            else if (SelectedCategory.Equals("1"))
            {
                return DialogMessagesErrorsAndConstants.CategoryElectricity;
            }
            else if (SelectedCategory.Equals("2"))
            {
                return DialogMessagesErrorsAndConstants.CategoryHeating;
            }
            else if (SelectedCategory.Equals("3"))
            {
                return DialogMessagesErrorsAndConstants.CategoryFood;
            }
            else if (SelectedCategory.Equals("4"))
            {
                return DialogMessagesErrorsAndConstants.CategoryClothing;
            }
            else
            {
                return DialogMessagesErrorsAndConstants.CategoryMisc; 
            }
        }

        public void GetPayment()
        {
            Payment.UserId = ApplicationResourcesUtility.GetLoggedOnUser().Id;
            Payment.Amount = Amount;
            Payment.Category = GetCategory();
            Payment.DateIssued = DateTime.Now;
            Payment.RecipientName = SelectedHouseMate.FullName;
            Payment.BillToId = SelectedHouseMate.Id;
            Payment.BillFromName = ApplicationResourcesUtility.GetLoggedOnUser().FullName;
            Payment.PaymentStatus = PaymentStatus.Unpaid;
            Payment.DatePaid = DateTime.Now;
        }

        public List<Payment> GetPaymentForAllHousemates()
        {
            List<Payment> _allPayments = new List<Payment>();

            foreach (HouseMateResult housemate in HouseMates.Where(h => h.FirstName != DialogMessagesErrorsAndConstants.PayAllHousemates))
            {
                    Payment.UserId = ApplicationResourcesUtility.GetLoggedOnUser().Id;
                    Payment.Amount = Amount;
                    Payment.Category = GetCategory();
                    Payment.DateIssued = DateTime.Today;
                    Payment.RecipientName = housemate.FullName;
                    Payment.BillToId = housemate.Id;
                    Payment.BillFromName = ApplicationResourcesUtility.GetLoggedOnUser().FullName;
                    Payment.PaymentStatus = PaymentStatus.Unpaid;
                    _allPayments.Add(Payment);
            }

            return _allPayments;
        }

        public bool ValidateInput()
        {
            bool[] validation = new bool[3];
            validation[0] = Amount == 0 ? true : false;
            validation[1] = ValidationUtility.NullOrEmpty(SelectedCategory);
            validation[2] = ValidationUtility.NullOrEmptyObject(SelectedHouseMate);

            int errorCount = 0;

            foreach (bool b in validation)
            {
                if (b)
                {
                    errorCount++;
                }
            }

            if (errorCount > 1)
            {
                DialogUtility.ShowMobileMessageDialog(DialogMessagesErrorsAndConstants.MultipleErrorsOnPage);
                return false;
            }

            if (validation[0])
            {
                DialogUtility.ShowMobileMessageDialog(DialogMessagesErrorsAndConstants.PleaseAddAmountToBill);
                return false;
            }

            if (validation[1])
            {
                DialogUtility.ShowMobileMessageDialog(DialogMessagesErrorsAndConstants.PleaseSelectACategory);
                return false;
            }

            if (validation[2])
            {
                DialogUtility.ShowMobileMessageDialog(DialogMessagesErrorsAndConstants.PleaseSelectAHousemate);
                return false;
            }

            return true;
        }
    }
}
