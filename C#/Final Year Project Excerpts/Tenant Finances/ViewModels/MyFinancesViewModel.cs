﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using HomeKeeper.Models;
using HomeKeeper.Utilities;
using HomeKeeper.Utilities.Constants;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;
using Windows.UI.Xaml;

namespace HomeKeeper.ViewModels
{
    public class MyFinancesViewModel : ViewModelBase
    {

        private List<Payment> _sentBills = new List<Payment>();

        public List<Payment> SentBills
        {
            get { return _sentBills; }
            set
            {
                if (_sentBills != value)
                {
                    _sentBills = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Payment _selectedSentBill;
        public Payment SelectedSentBill
        {
            get { return _selectedSentBill; }
            set
            {
                if (_selectedSentBill != value)
                {
                    _selectedSentBill = value;
                    ApplicationResourcesUtility.SetSelectedSentBill(_selectedSentBill);
                    MarkedAsPaidVisible = Visibility.Visible;
                    ClearPaidVisible = Visibility.Visible;
                    RaisePropertyChanged();
                }
            }
        }

        private String _fetchSentBillsEmptyText;
        public String FetchSentBillsEmptyText
        {
            get { return _fetchSentBillsEmptyText; }
            set
            {
                if (_fetchSentBillsEmptyText != value)
                {
                    _fetchSentBillsEmptyText = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Visibility _sentBillsEmptyVisible;
        public Visibility SentBillsEmptyVisible
        {
            get { return _sentBillsEmptyVisible; }
            set
            {
                if (_sentBillsEmptyVisible != value)
                {
                    _sentBillsEmptyVisible = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Visibility _sentBillsVisible;

        public Visibility SentBillsVisible
        {
            get { return _sentBillsVisible; }
            set
            {
                if (_sentBillsVisible != value)
                {
                    _sentBillsVisible = value;
                    RaisePropertyChanged();
                }
            }
        }

        private List<Payment> _receivedBills = new List<Payment>();

        public List<Payment> ReceivedBills
        {
            get { return _receivedBills; }
            set
            {
                if (_receivedBills != value)
                {
                    _receivedBills = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Payment _selectedReceivedBill;

        public Payment SelectedReceivedBill
        {
            get { return _selectedReceivedBill; }
            set
            {
                if (_selectedReceivedBill != value)
                {
                    _selectedReceivedBill = value;
                    ApplicationResourcesUtility.SetSelectedReceivedBill(_selectedReceivedBill);
                    RaisePropertyChanged();
                }
            }
        }


        private Visibility _receivedBillsVisible;
        public Visibility ReceivedBillsVisible
        {
            get { return _receivedBillsVisible; }
            set
            {
                if (_receivedBillsVisible != value)
                {
                    _receivedBillsVisible = value;
                    RaisePropertyChanged();
                }
            }
        }

        private String _fetchReceivedBillsEmptyText;
        public String FetchReceivedBillsEmptyText
        {
            get { return _fetchReceivedBillsEmptyText; }
            set
            {
                if (_fetchReceivedBillsEmptyText != value)
                {
                    _fetchReceivedBillsEmptyText = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Visibility _receivedBillsEmptyVisible;
        public Visibility ReceivedBillsEmptyVisible
        {
            get { return _receivedBillsEmptyVisible; }
            set
            {
                if (_receivedBillsEmptyVisible != value)
                {
                    _receivedBillsEmptyVisible = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Visibility _markedAsPaidVisible = Visibility.Collapsed;

        public Visibility MarkedAsPaidVisible
        {
            get { return _markedAsPaidVisible; }
            set
            {
                if (_markedAsPaidVisible != value)
                {
                    _markedAsPaidVisible = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Visibility _clearPaidVisible = Visibility.Collapsed;

        public Visibility ClearPaidVisible
        {
            get { return _clearPaidVisible; }
            set
            {
                if (_clearPaidVisible != value)
                {
                    _clearPaidVisible = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Boolean _createBillEnabled;

        public Boolean CreateBillEnabled
        {
            get { return _createBillEnabled; }
            set
            {
                if (_createBillEnabled != value)
                {
                    _createBillEnabled = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Boolean _billPaidEnabled;

        public Boolean BillPaidEnabled
        {
            get { return _billPaidEnabled; }
            set
            {
                if (_billPaidEnabled != value)
                {
                    _billPaidEnabled = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Boolean _clearPaidBillsEnabled;

        public Boolean ClearPaidBillsEnabled
        {
            get { return _clearPaidBillsEnabled; }
            set
            {
                if (_clearPaidBillsEnabled != value)
                {
                    _clearPaidBillsEnabled = value;
                    RaisePropertyChanged();
                }
            }
        }


        public RelayCommand HomeCommand { get; set; }
        public RelayCommand ProfileCommand { get; set; }
        public RelayCommand LogoutCommand { get; set; }
        public RelayCommand CreateBillCommand { get; set; }

        public RelayCommand MarkBillAsPaidCommand { get; set; }

        public RelayCommand ClearPaidBillsCommand { get; set; }

        public MyFinancesViewModel()
        {
            SetupNavigation();
            SetupCommands();
            ClearPaidBillsEnabled = true;
            CreateBillEnabled = true;
            BillPaidEnabled = true;
        }

        public void SetupNavigation()
        {
            HomeCommand = new RelayCommand(NavigationUtility.NavigateToHome);
            ProfileCommand = new RelayCommand(NavigationUtility.NavigateToProfile);
            LogoutCommand = new RelayCommand(NavigationUtility.NavigateToLogin);
            CreateBillCommand = new RelayCommand(NavigationUtility.NavigateToCreateBill);
            MarkBillAsPaidCommand = new RelayCommand(MarkBillAsPaid);
            ClearPaidBillsCommand = new RelayCommand(ClearPaidBills);
        }

        public void SetupCommands()
        {
            GetSentBills();
            GetReceivedBills();
        }

        public async void ClearPaidBills()
        {
            if (NetworkInformation.GetInternetConnectionProfile() != null)
            {
                try
                {
                    if (SelectedSentBill != null)
                    {
                        ClearPaidBillsEnabled = false;
                        CreateBillEnabled = false;
                        BillPaidEnabled = false;

                        int counter = 0;
                        foreach (Payment payment in SentBills)
                        {
                            if (payment.PaymentStatus == PaymentStatus.Paid)
                            {
                                await App.PaymentTable.DeleteAsync(payment);
                                counter++;
                            }
                        }
                        if (counter > 0)
                        {
                            DialogUtility.ShowMobileMessageDialog(DialogMessagesErrorsAndConstants.PaidBillsCleared);
                            NavigationUtility.NavigateToMyProperty();
                        }
                        else
                        {
                            DialogUtility.ShowMobileMessageDialog(DialogMessagesErrorsAndConstants.NoPaidBillsFound);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                    DialogUtility.ShowMessage(DialogMessagesErrorsAndConstants.ErrorOccured);
                }
            }
            else
            {
                DialogUtility.NoNetworkConnection();
            }
        }

        public async void MarkBillAsPaid()
        {
            if (NetworkInformation.GetInternetConnectionProfile() != null)
            {
                try
                {
                    if (SelectedSentBill != null)
                    {
                        if (SelectedSentBill.PaymentStatus == PaymentStatus.Unpaid)
                        {
                            SelectedSentBill.PaymentStatus = PaymentStatus.Paid;
                        }
                        await App.PaymentTable.UpdateAsync(SelectedSentBill);
                        DialogUtility.ShowMobileMessageDialog(DialogMessagesErrorsAndConstants.BillPaid);
                        NavigationUtility.NavigateToMyProperty();
                    }         
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                    DialogUtility.ShowMessage(DialogMessagesErrorsAndConstants.ErrorOccured);
                }
            }
            else
            {
                DialogUtility.NoNetworkConnection();
            }
        }

        public async void GetSentBills()
        {
            if (NetworkInformation.GetInternetConnectionProfile() != null)
            {
                try
                {
                    List<Payment> sentBills = await App.PaymentTable.Where(transaction => transaction.UserId ==
                    ApplicationResourcesUtility.GetLoggedOnUser().Id).ToListAsync();

                    if (sentBills.Count > 0)
                    {
                        SentBillsVisible  = Visibility.Visible;
                        SentBillsEmptyVisible = Visibility.Collapsed;
                        SentBills = sentBills;
                    }
                    else
                    {
                        SelectedSentBill = null;
                        SentBillsVisible = Visibility.Collapsed;
                        SentBillsEmptyVisible = Visibility.Visible;
                        FetchSentBillsEmptyText = DialogMessagesErrorsAndConstants.NoMatchedSentBills;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                    DialogUtility.ShowMessage(DialogMessagesErrorsAndConstants.ErrorOccured);
                }
            }
            else
            {
                DialogUtility.NoNetworkConnection();
            }
        }

        public async void GetReceivedBills()
        {
            if (NetworkInformation.GetInternetConnectionProfile() != null)
            {
                try
                {
                    List<Payment> receivedBills = await App.PaymentTable.Where(transaction => transaction.BillToId == ApplicationResourcesUtility.GetLoggedOnUser().Id &&
                        transaction.PaymentStatus == PaymentStatus.Unpaid).ToListAsync();

                    if (receivedBills.Count > 0)
                    {
                        ReceivedBillsVisible = Visibility.Visible;
                        ReceivedBillsEmptyVisible = Visibility.Collapsed;
                        ReceivedBills = receivedBills;
                    }
                    else
                    {
                        SelectedReceivedBill = null;
                        ReceivedBillsVisible = Visibility.Collapsed;
                        ReceivedBillsEmptyVisible = Visibility.Visible;
                        FetchReceivedBillsEmptyText = DialogMessagesErrorsAndConstants.NoMatchedReceivedBills;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                    DialogUtility.ShowMessage(DialogMessagesErrorsAndConstants.ErrorOccured);
                }
            }
            else
            {
                DialogUtility.NoNetworkConnection();
            }
        }
    }
}
