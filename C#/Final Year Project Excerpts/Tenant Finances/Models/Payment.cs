﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace HomeKeeper.Models
{
    public class Payment
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "userId")]
        public string UserId { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public double Amount { get; set; }

        [JsonProperty(PropertyName = "category")]
        public string Category { get; set; }

        [JsonProperty(PropertyName = "billToId")]
        public string BillToId { get; set; }

        [JsonProperty(PropertyName = "billFromName")]
        public string BillFromName { get; set; }

        [JsonProperty(PropertyName = "recipientName")]
        public string RecipientName { get; set; }

        [JsonProperty(PropertyName = "paymentStatus")]
        public PaymentStatus PaymentStatus { get; set; }

        [JsonProperty(PropertyName = "dateIssued")]
        public DateTime DateIssued { get; set; }

        [JsonProperty(PropertyName = "datePaid")]
        public DateTime DatePaid { get; set; }

        public string AmountFormatted
        {
            get { return "Amount: £" + Amount; }
        }

        public string CategoryFormatted
        {
            get { return "Category: " + Category; }
        }

        public string DateIssuedFormatted
        {
            get { return "Date Issued: " + Utilities.DateTimeFormatUtility.FormatDateToShortDate(DateIssued); }
        }

        public string PaymentStatusFormatted
        {
            get { return "Payment Status: " + PaymentStatus; }
        }

    }

    public enum PaymentStatus
    {
        Unpaid,
        Paid
    }


}
