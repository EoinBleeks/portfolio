﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml;
using GalaSoft.MvvmLight;
using HomeKeeper.Utilities;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;

namespace HomeKeeper.Models
{
    public class Property : ViewModelBase
    {
        public Property(bool test = false)
        {
            if (!test && Window.Current != null)
            {
                _width = Window.Current.Bounds.Width - 100;
            }
        }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "addressLine1")]
        public string AddressLine1 { get; set; }

        [JsonProperty(PropertyName = "addressLine2")]
        public string AddressLine2 { get; set; }

        [JsonProperty(PropertyName = "addressLine3")]
        public string AddressLine3 { get; set; }

        [JsonProperty(PropertyName = "cityTown")]
        public string CityTown { get; set; }

        [JsonProperty(PropertyName = "postcode")]
        public string Postcode { get; set; }

        [JsonProperty(PropertyName = "deposit")]
        public double Deposit { get; set; }

        [JsonProperty(PropertyName = "monthlyRent")]
        public double MonthlyRent { get; set; }

        [JsonProperty(PropertyName = "includesRates")]
        public bool IncludesRates { get; set; }

        [JsonProperty(PropertyName = "viewableFrom")]
        public DateTime ViewableFrom { get; set; }

        [JsonProperty(PropertyName = "availableFrom")]
        public DateTime AvailableFrom { get; set; }

        [JsonProperty(PropertyName = "numBedrooms")]
        public int NumBedrooms { get; set; }

        [JsonProperty(PropertyName = "numBathrooms")]
        public int NumBathrooms { get; set; }

        [JsonProperty(PropertyName = "heating")]
        public string Heating { get; set; }

        [JsonProperty(PropertyName = "rentalStatus")]
        public string RentalStatus { get; set; }

        [JsonProperty(PropertyName = "furnished")]
        public bool Furnished { get; set; }

        [JsonProperty(PropertyName = "additionalDescription")]
        public string AdditionalDescription { get; set; }

        [JsonProperty(PropertyName = "managerId")]
        public string ManagerId { get; set; }

        [JsonProperty(PropertyName = "latitude")]
        public string Latitude { get; set; }

        [JsonProperty(PropertyName = "longitude")]
        public string Longitude { get; set; }

        [JsonProperty(PropertyName = "numViewings")]
        public string NumViewings { get; set; }

        [JsonProperty(PropertyName = "primaryPicture")]
        public string PrimaryPicture { get; set; }

        [CreatedAt]
        public DateTimeOffset CreatedAt { get; set; }

        public string MonthlyRentFormatted
        {
            get { return "£" + MonthlyRent; }
        }

        public string DepositFormatted
        {
            get { return "£" + Deposit; }
        }

        public string FullAddress
        {
            get { return AddressLine1 + ", " + CityTown + ", " + Postcode; }
        }

        public string RatesFormatted
        {
            get { return IncludesRates ? "Rates included" : "Rates Excluded"; }
        }

        public string FurnishedFormatted
        {
            get { return IncludesRates ? "Furnished" : "Not Furnished"; }
        }

        public string AvailableFromFormatted
        {
            get { return DateTimeFormatUtility.FormatDateToShortDate(AvailableFrom); }
        }

        public string ViewableFromFormatted
        {
            get { return DateTimeFormatUtility.FormatDateToShortDate(ViewableFrom); }
        }

        public string AdditionalDescriptionFormatted
        {
            get { return AdditionalDescription ?? "N/A"; }
        }

        private double _width;
        public double Width
        {
            get { return _width; }
            set
            {
                _width = value;
                RaisePropertyChanged();
            }
        }
    }
}
