﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;
using Windows.UI.Xaml;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using HomeKeeper.Models;
using HomeKeeper.Utilities;
using HomeKeeper.Utilities.Constants;

namespace HomeKeeper.ViewModels
{
    public class PropertySearchViewModel : ViewModelBase
    {
        private PropertySearchCriteria _propertySearchCriteria = new PropertySearchCriteria();
        private List<Property> _properties = new List<Property>();

        public List<Property> Properties
        {
            get { return _properties; }
            set
            {
                if (_properties != value)
                {
                    _properties = value;
                    RaisePropertyChanged();
                }
            }
        }


        private Visibility _propertiesVisible;

        public Visibility PropertiesVisible
        {
            get { return _propertiesVisible; }
            set
            {
                if (_propertiesVisible != value)
                {
                    _propertiesVisible = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Visibility _emptyVisible;

        public Visibility EmptyVisible
        {
            get { return _emptyVisible; }
            set
            {
                if (_emptyVisible != value)
                {
                    _emptyVisible = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Property _selectedProperty;

        public Property SelectedProperty
        {
            get { return _selectedProperty; }
            set
            {
                if (_selectedProperty != value)
                {
                    _selectedProperty = value;
                    ApplicationResourcesUtility.SetSelectedProperty(_selectedProperty);
                    NavigationUtility.NavigateToPropertyDetails();
                    RaisePropertyChanged();
                }
            }
        }

        private string _fetchEmptyText = DialogMessagesErrorsAndConstants.FetchingSearchResults;

        public string FetchEmptyText
        {
            get { return _fetchEmptyText; }
            set
            {
                if (_fetchEmptyText != value)
                {
                    _fetchEmptyText = value;
                    RaisePropertyChanged();
                }
            }
        }

        public RelayCommand HomeCommand { get; set; }
        public RelayCommand ProfileCommand { get; set; }
        public RelayCommand LogoutCommand { get; set; }

        public PropertySearchViewModel()
        {
            SetupNavigation();
            GetSearchProperties();
        }

        private void SetupNavigation()
        {
            LogoutCommand = new RelayCommand(NavigationUtility.NavigateToLogin);
            ProfileCommand = new RelayCommand(NavigationUtility.NavigateToProfileUpdateDetails);
            HomeCommand = new RelayCommand(NavigationUtility.NavigateToHome);
        }

        private async void GetSearchProperties()
        {
            if (NetworkInformation.GetInternetConnectionProfile() != null)
            {
                try
                {
                    _propertySearchCriteria = ApplicationResourcesUtility.GetPropertySearchResults();

                    List<Property> propertyList = await App.PropertyTable.Where(property =>
                        (_propertySearchCriteria.CityTown == null || property.CityTown == _propertySearchCriteria.CityTown)
                        && (_propertySearchCriteria.Postcode == null || property.Postcode.Substring(0, 4) == _propertySearchCriteria.Postcode)
                        && (_propertySearchCriteria.MinimumMonthlyRent == 0.0 || property.MonthlyRent >= _propertySearchCriteria.MinimumMonthlyRent)
                        && (_propertySearchCriteria.MaximumMonthlyRent == 0.0 || property.MonthlyRent <= _propertySearchCriteria.MaximumMonthlyRent)
                        && (_propertySearchCriteria.NumBedrooms == 0 || property.NumBedrooms == _propertySearchCriteria.NumBedrooms)
                        && (_propertySearchCriteria.NumBathrooms == 0 || property.NumBathrooms == _propertySearchCriteria.NumBathrooms)
                        && (_propertySearchCriteria.Furnished == false || property.Furnished == _propertySearchCriteria.Furnished)
                        && (_propertySearchCriteria.Heating == null || property.Heating == _propertySearchCriteria.Heating)
                        ).OrderByDescending(property => property.CreatedAt).ToListAsync();


                    if (propertyList.Count > 0)
                    {
                        PropertiesVisible = Visibility.Visible;
                        EmptyVisible = Visibility.Collapsed;
                        Properties = propertyList;
                    }
                    else
                    {
                        SelectedProperty = null;
                        PropertiesVisible = Visibility.Collapsed;
                        EmptyVisible = Visibility.Visible;
                        FetchEmptyText = DialogMessagesErrorsAndConstants.NoMatchedProperties;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                    DialogUtility.ShowMessage(DialogMessagesErrorsAndConstants.ErrorOccured);
                }
            }
            else
            {
                DialogUtility.NoNetworkConnection();
            }           
        }
    }
}
