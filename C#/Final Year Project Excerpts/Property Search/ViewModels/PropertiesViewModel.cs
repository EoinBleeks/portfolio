﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
//using Windows.Graphics.Printing.OptionDetails;
using Windows.Networking.Connectivity;
using Windows.UI.Xaml;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HomeKeeper.Models;
using HomeKeeper.Utilities;
using HomeKeeper.Utilities.Constants;
using Microsoft.WindowsAzure.MobileServices;

namespace HomeKeeper.ViewModels
{
    public class PropertiesViewModel : ViewModelBase
    {
        private PropertySearchCriteria _propertySearchCriteria = new PropertySearchCriteria();

        public PropertySearchCriteria PropertySearchCriteria
        {
            get { return _propertySearchCriteria; }
            set
            {
                if (_propertySearchCriteria != value)
                {
                    _propertySearchCriteria = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _cityTown;
        public string CityTown
        {
            get { return _cityTown; }
            set
            {
                if (_cityTown != value)
                {
                    _cityTown = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _postcode;
        public string Postcode
        {
            get { return _postcode; }
            set
            {
                if (_postcode != value)
                {
                    _postcode = value;
                    RaisePropertyChanged();
                }
            }
        }

        private List<string> _furnishedValues = new List<string>();

        public List<string> FurnishedValues
        {
            get { return _furnishedValues; }
            set
            {
                if (_furnishedValues != value)
                {
                    _furnishedValues = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _furnished;
        public string Furnished
        {
            get { return _furnished; }
            set
            {
                if (_furnished != value)
                {
                    _furnished = value;
                    RaisePropertyChanged();
                }
            }
        }

        List<string> _heatingValues = new List<string>();

        public List<string> HeatingValues
        {
            get { return _heatingValues; }
            set
            {
                if (_heatingValues != value)
                {
                    _heatingValues = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _heating;
        public string Heating
        {
            get { return _heating; }
            set
            {
                if (_heating != value)
                {
                    _heating = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _rentalStatus;
        public string RentalStatus
        {
            get { return _rentalStatus; }
            set
            {
                if (_rentalStatus != value)
                {
                    _rentalStatus = value;
                    RaisePropertyChanged();
                }
            }
        }
        private DateTime _viewableFrom;
        public DateTime ViewableFrom
        {
            get { return _viewableFrom; }
            set
            {
                if (_viewableFrom != value)
                {
                    _viewableFrom = value;
                    RaisePropertyChanged();
                }
            }
        }
        private DateTime _availableFrom;
        public DateTime AvailableFrom
        {
            get { return _availableFrom; }
            set
            {
                if (_availableFrom != value)
                {
                    _availableFrom = value;
                    RaisePropertyChanged();
                }
            }
        }
        

        private double _minMonthlyRent;
        public double MinMonthlyRent
        {
            get { return _minMonthlyRent; }
            set
            {
                if (_minMonthlyRent != value)
                {
                    _minMonthlyRent = value;
                    RaisePropertyChanged();
                }
            }
        }

        private double _maxMonthlyRent;

        public double MaxMonthlyRent
        {
            get { return _maxMonthlyRent; }
            set
            {
                if (_maxMonthlyRent != value)
                {
                    _maxMonthlyRent = value;
                    RaisePropertyChanged();
                }
            }
        }

        private int _numOfBedrooms;

        public int NumBedrooms
        {
            get { return _numOfBedrooms; }
            set
            {
                if (_numOfBedrooms != value)
                {
                    _numOfBedrooms = value;
                    RaisePropertyChanged();
                }
            }
        }

        private int _numOfBathrooms;

        public int NumBathrooms
        {
            get { return _numOfBathrooms; }
            set
            {
                if (_numOfBathrooms != value)
                {
                    _numOfBathrooms = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _errorMessage;
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                if (_errorMessage != value)
                {
                    _errorMessage = value;
                    RaisePropertyChanged();
                }
            }
        }

        public RelayCommand SearchResultsPageCommand { get; set; }
        public RelayCommand HomeCommand { get; set; }
        public RelayCommand ProfileCommand { get; set; }
        public RelayCommand LogoutCommand { get; set; }

        public PropertiesViewModel()
        {
            SetupComboBoxValues();
            _viewableFrom = DateTime.Today;
            _availableFrom = DateTime.Today; 
            SearchResultsPageCommand = new RelayCommand(NavigateToSearchResults);
            SetupNavigation();
        }

        private void SetupNavigation()
        {
            HomeCommand = new RelayCommand(NavigationUtility.NavigateToHome);
            ProfileCommand = new RelayCommand(NavigationUtility.NavigateToProfileUpdateDetails);
            LogoutCommand = new RelayCommand(NavigationUtility.NavigateToLogin);
        }

        public void SetupComboBoxValues()
        {
            _heatingValues.Add(DialogMessagesErrorsAndConstants.Oil);
            _heatingValues.Add(DialogMessagesErrorsAndConstants.Gas);
            _heatingValues.Add(DialogMessagesErrorsAndConstants.Electric);
            _furnishedValues.Add(DialogMessagesErrorsAndConstants.Yes);
            _furnishedValues.Add(DialogMessagesErrorsAndConstants.No);
        }

        private void NavigateToSearchResults()
        {
            SetSearchCriteria();
            if (!SearchInputValidation())
            {
                ApplicationResourcesUtility.SetPropertySearchResults(_propertySearchCriteria);
                NavigationUtility.NavigateToPropertySearchResults();
            }
        }

        public void SetSearchCriteria()
        {
            _propertySearchCriteria.CityTown = CityTown;
            _propertySearchCriteria.Postcode = Postcode;
            _propertySearchCriteria.NumBedrooms = NumBedrooms;
            _propertySearchCriteria.NumBathrooms = NumBathrooms;
            _propertySearchCriteria.MaximumMonthlyRent = MaxMonthlyRent;
            _propertySearchCriteria.MinimumMonthlyRent = MinMonthlyRent;
            if(Furnished == "0" || Furnished == null)
            {
                _propertySearchCriteria.Furnished = false;
            }
            else
            {
                _propertySearchCriteria.Furnished = true;
            }
            _propertySearchCriteria.Heating = Heating;
            _propertySearchCriteria.ViewableFrom = ViewableFrom;
            _propertySearchCriteria.AvailableFrom = AvailableFrom;
            
        }


        private bool SearchInputValidation()
        {
            if (Postcode != null)
            {
                if (!ValidationUtility.ValidatePostcode(Postcode))
                {
                    DialogUtility.ShowMobileMessageDialog(DialogMessagesErrorsAndConstants.EnterValidPostcode);
                    return true;
                }
            }
            return false;

        }   
    }
}