﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Devices.Geolocation;
using Windows.Networking.Connectivity;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Flurl;
using Flurl.Http;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using HomeKeeper.Models;
using HomeKeeper.Utilities;
using HomeKeeper.Utilities.Constants;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http;
using HomeKeeper.Utilities.Map;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HomeKeeper.ViewModels
{
    public class PropertyDetailsViewModel : ViewModelBase
    {
        private Property _selectedProperty;
        private string _coOrdinates;
        private double _averageRatingCalculated;


        public Property SelectedProperty
        {
            get { return _selectedProperty; }
            set
            {
                if (_selectedProperty != value)
                {
                    _selectedProperty = value;
                    RaisePropertyChanged();
                }
            }
        }

        private List<UploadedPropertyFile> _propertyImages;

        public List<UploadedPropertyFile> PropertyImages
        {
            get { return _propertyImages; }
            set
            {
                if (_propertyImages != value)
                {
                    _propertyImages = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Visibility _propertyImagesVisible;

        public Visibility PropertyImagesVisible
        {
            get { return _propertyImagesVisible; }
            set
            {
                if (_propertyImagesVisible != value)
                {
                    _propertyImagesVisible = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Visibility _imagesEmptyVisible;

        public Visibility ImagesEmptyVisible
        {
            get { return _imagesEmptyVisible; }
            set
            {
                if (_imagesEmptyVisible != value)
                {
                    _imagesEmptyVisible = value;
                    RaisePropertyChanged();
                }
            }
        }
        
        public string AverageRating
        {
            get { return "Average Rating: " + _averageRatingCalculated + "/5"; }
        }

        private List<PropertyReviewResult> _propertyReviews = new List<PropertyReviewResult>();


        public List<PropertyReviewResult> PropertyReviews
        {
            get { return _propertyReviews; }
            set
            {
                if (_propertyReviews != value)
                {
                    _propertyReviews = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Visibility _reviewsVisible;

        public Visibility ReviewsVisible
        {
            get { return _reviewsVisible; }
            set
            {
                if (_reviewsVisible != value)
                {
                    _reviewsVisible = value;
                    RaisePropertyChanged();
                }
            }
        }

        public string AddressField
        {
            get { return _selectedProperty.AddressLine1; }
        }

        public string Furnishings
        {
            get 
            {
                return _selectedProperty.Furnished ? DialogMessagesErrorsAndConstants.PropertyFurnished : DialogMessagesErrorsAndConstants.PropertyUnfurnished;
            }
        }


        public string Rent
        {
            get { return _selectedProperty.MonthlyRent.ToString(); }
        }

        public string Deposit
        {
            get { return _selectedProperty.Deposit.ToString(); }
        }

        public string ViewableFrom
        {
            get { return DateTimeFormatUtility.FormatDateToShortDate(_selectedProperty.ViewableFrom); }
        }

        public string AvailableFrom
        {
            get { return DateTimeFormatUtility.FormatDateToShortDate(_selectedProperty.AvailableFrom); }
        }

        public string RentStatus
        {
            get { return _selectedProperty.RentalStatus; }
        }

        public string Bedrooms
        {
            get { return _selectedProperty.NumBedrooms.ToString(); }
        }

        public string Bathrooms
        {
            get { return _selectedProperty.NumBathrooms.ToString(); }
        }

        public string Heating
        {
            get { return _selectedProperty.Heating; }
        }
        public string Rates
        {
            get
            {
                return  _selectedProperty.IncludesRates ? DialogMessagesErrorsAndConstants.RatesIncluded : DialogMessagesErrorsAndConstants.RatesExcluded;
            }
        }

        public String PropertyDescription
        {
            get { return "\nProperty Description:\n" + _selectedProperty.AdditionalDescription; }
        }

        private Visibility _addApplicationVisible;
        public Visibility AddApplicationVisible
        {
            get { return _addApplicationVisible; }
            set
            {
                if (_addApplicationVisible != value)
                {
                    _addApplicationVisible = value;
                    RaisePropertyChanged();
                }
            }
        }

        private bool _isChecked = false;

        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                if (_isChecked != value)
                {
                    _isChecked = value;
                    RaisePropertyChanged();
                }
            }
        }

        private bool _isEnabled = true;

        public bool IsEnabled
        {
            get {return _isEnabled; }
            set
            {
                if (_isEnabled != value)
                {
                    _isEnabled = value;
                    RaisePropertyChanged();
                }
            }
        }

        private bool _directionsEnabled = true;

        public bool DirectionsEnabled
        {
            get { return _directionsEnabled; }
            set
            {
                if (_directionsEnabled != value)
                {
                    _directionsEnabled = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Visibility _directionsVisible;
        public Visibility DirectionsVisible
        {
            get { return _directionsVisible; }
            set
            {
                if (_directionsVisible != value)
                {
                    _directionsVisible = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Visibility _actionsVisible;
        public Visibility ActionsVisible
        {
            get { return _actionsVisible; }
            set
            {
                if (_actionsVisible != value)
                {
                    _actionsVisible = value;
                    RaisePropertyChanged();
                }
            }
        }
        
        private Visibility _popupMessageVisible;

        public Visibility PopupMessageVisible
        {
            get { return _popupMessageVisible; }
            set
            {
                if (_popupMessageVisible != value)
                {
                    _popupMessageVisible = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _popupMessage = DialogMessagesErrorsAndConstants.FetchingDirections;
        public string PopupMessage
        {
            get { return _popupMessage; }
            set
            {
                if (_popupMessage != value)
                {
                    _popupMessage = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Visibility _reviewsEmptyVisible;

        public Visibility ReviewsEmptyVisible
        {
            get { return _reviewsEmptyVisible; }
            set
            {
                if (_reviewsEmptyVisible != value)
                {
                    _reviewsEmptyVisible = value;
                    RaisePropertyChanged();
                }
            }
        }


        private bool _popupIsOpen;
        public bool PopupIsOpen
        {
            get { return _popupIsOpen; }
            set
            {
                if (_popupIsOpen != value)
                {
                    _popupIsOpen = value;
                    RaisePropertyChanged();
                }
            }
        }

        private string _fetchEmptyText = DialogMessagesErrorsAndConstants.FetchingPropertyFiles;

        public string FetchEmptyText
        {
            get { return _fetchEmptyText; }
            set
            {
                if (_fetchEmptyText != value)
                {
                    _fetchEmptyText = value;
                    RaisePropertyChanged();
                }
            }
        }

        public RelayCommand LogoutCommand { get; set; }
        public RelayCommand ActivityLogCommand { get; set; }
        public RelayCommand HomeCommand { get; set; }
        public RelayCommand ProfileCommand { get; set; }
        public RelayCommand ArrangeViewingCommand { get; set; }
        public RelayCommand SubmitLeaseApplicationCommand { get; set; }
        public RelayCommand PropertyFilesCommand { get; set; }
        public RelayCommand FavouritePropertyCommand { get; set; }
        public RelayCommand DirectionsCommand { get; set; }
        
        public PropertyDetailsViewModel()
        {
            SelectedProperty = ApplicationResourcesUtility.GetSelectedProperty();
            AddApplicationVisible = Visibility.Collapsed;
            SetupNavigation();
            SetupCommands();
            AllowLeaseApplications();
            PropertyFavouritedValidation();
            ReturnListOfReviews();
            GetPropertyImages();
            PopupMessageVisible = Visibility.Collapsed;
        }

        private void SetupNavigation()
        {
            LogoutCommand = new RelayCommand(NavigationUtility.NavigateToLogin);
            ProfileCommand = new RelayCommand(NavigationUtility.NavigateToProfileUpdateDetails);
            ActivityLogCommand = new RelayCommand(NavigationUtility.NavigateToActivityLog);
            HomeCommand = new RelayCommand(NavigationUtility.NavigateToHome);
            ArrangeViewingCommand = new RelayCommand(NavigationUtility.NavigateToAddViewing);
            SubmitLeaseApplicationCommand = new RelayCommand(NavigationUtility.NavigateToSubmitLeaseApplication);
            PropertyFilesCommand = new RelayCommand(NavigationUtility.NavigateToUploadedPropertyFiles);
        }

        private void SetupCommands()
        {
            FavouritePropertyCommand = new RelayCommand(FavouriteCurrentProperty);
            DirectionsCommand = new RelayCommand(LaunchMap);
        }

        private async void GetPropertyImages()
        {
            if (NetworkInformation.GetInternetConnectionProfile() != null)
            {
                try
                {
                    List<UploadedPropertyFile> imageUris =
                        await
                            App.PropertyFilesTable.Where(
                                blob => blob.Type == BlobPathsAndTypes.Image && blob.PropertyId.ToString() == SelectedProperty.Id.ToString())
                                .OrderByDescending(blob => blob.CreatedAt)
                                .ToListAsync();

                    if (imageUris.Count > 0)
                    {
                        ImagesEmptyVisible = Visibility.Collapsed;
                        PropertyImagesVisible = Visibility.Visible;
                        PropertyImages = imageUris;
                    }

                    else
                    {
                        PropertyImages = null;
                        PropertyImagesVisible = Visibility.Collapsed;
                        ImagesEmptyVisible = Visibility.Visible;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                    DialogUtility.ShowMessage(DialogMessagesErrorsAndConstants.PleaseTryAgain,
                        DialogMessagesErrorsAndConstants.ReviewsError);
                }
            }
            else
            {
                ImagesEmptyVisible = Visibility.Visible;
                DialogUtility.NoNetworkConnection();
            }
        }

        private async void ReturnListOfReviews()
        {
            if (NetworkInformation.GetInternetConnectionProfile() != null)
            {
                try
                {
                    string controller = DialogMessagesErrorsAndConstants.PropertyReviewController;

                    JArray result = await App.MobileService
                            .InvokeApiAsync<JArray>(controller,
                                System.Net.Http.HttpMethod.Get, new Dictionary<string, string>
                                                    {
                                                        {"propertyId", SelectedProperty.Id}
                                                    });

                    PropertyReviews = result.ToObject<List<PropertyReviewResult>>().ToList();

                    //PropertyReviews.Sort();
                  
                    if (PropertyReviews.Count > 0)
                    {
                        ReviewsVisible = Visibility.Visible;
                        ReviewsEmptyVisible = Visibility.Collapsed;
                        
                        foreach (var propertyReview in PropertyReviews)
                        {
                            _averageRatingCalculated += propertyReview.RatingNumber;
                        }

                        _averageRatingCalculated /= PropertyReviews.Count;

                    }
                    else
                    {
                        ReviewsVisible = Visibility.Collapsed;
                        FetchEmptyText = DialogMessagesErrorsAndConstants.NoReviewsFound;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                    DialogUtility.ShowMessage(DialogMessagesErrorsAndConstants.PleaseTryAgain,
                        DialogMessagesErrorsAndConstants.ReviewsError);
                }
                //PopupIsOpen = false;
            }
            else
            {
                ReviewsEmptyVisible = Visibility.Visible;
                DialogUtility.NoNetworkConnection();
            }
        }

        private async void AllowLeaseApplications()
        {
            if (NetworkInformation.GetInternetConnectionProfile() != null)
            {
                try
                {
                    List<LeaseTenant> leasesForUser =
                        await
                            App.LeaseTenantTable.Where(
                                lease => lease.TenantId == ApplicationResourcesUtility.GetLoggedOnUser().Id).ToListAsync();

                    AddApplicationVisible = leasesForUser.Count > 0 ? Visibility.Collapsed : Visibility.Visible;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                    DialogUtility.ShowMessage(DialogMessagesErrorsAndConstants.ErrorOccured);
                }
            }
            else
            {
                DialogUtility.NoNetworkConnection();
            }
        }

        private async void FavouriteCurrentProperty()
        {
            if (NetworkInformation.GetInternetConnectionProfile() != null)
            {
                try
                {
                    FavouriteProperty favouriteProperty = new FavouriteProperty();
                    favouriteProperty.PropertyId = SelectedProperty.Id;
                    favouriteProperty.UserId = ApplicationResourcesUtility.GetLoggedOnUser().Id;

                    List<FavouriteProperty> property =
                        await App.FavouritePropertiesTable.Where(prop => prop.UserId == ApplicationResourcesUtility.GetLoggedOnUser().Id &&
                            prop.PropertyId == SelectedProperty.Id).Take(1).ToListAsync();
                    //remove favourited property
                    if (property.Count > 0 && !property.First().Deleted)
                    {
                        favouriteProperty = property.First();
                        favouriteProperty.Deleted = true;

                        IsEnabled = false;
                        //remove entirely for the minute. TO-DO: use soft delete
                        await App.FavouritePropertiesTable.DeleteAsync(favouriteProperty);
                        //await App.FavouritePropertiesTable.UpdateAsync(favouriteProperty);
                        DialogUtility.ShowMobileMessageDialog(DialogMessagesErrorsAndConstants.PropertyUnfavouritedSmall,
                            DialogMessagesErrorsAndConstants.PropertyUnfavourited);
                        IsEnabled = true;
                    }
                    else
                    {
                        IsEnabled = false;
                        await App.FavouritePropertiesTable.InsertAsync(favouriteProperty);
                        DialogUtility.ShowMobileMessageDialog(DialogMessagesErrorsAndConstants.PropertyFavouritedSmall,
                            DialogMessagesErrorsAndConstants.PropertyFavourited);
                        IsEnabled = true;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                    DialogUtility.ShowMobileMessageDialog(DialogMessagesErrorsAndConstants.ErrorOccured);
                }
            }
            else
            {
                DialogUtility.ShowMobileMessageDialog(DialogMessagesErrorsAndConstants.NoInternetConnection, DialogMessagesErrorsAndConstants.ValidNetworkConnection);
            }
            
        }

        private async void LaunchMap()
        {
            ShowPopupAndDisable();
            Geolocator geolocator = new Geolocator();
            Geoposition currentLocation;
            try
            {
                currentLocation = await geolocator.GetGeopositionAsync();
                string currentAddress = await AddressToCooridinatesGenerator.GetAddressFromLatLong(currentLocation.Coordinate.Latitude.ToString(),
                    currentLocation.Coordinate.Longitude.ToString());
                string uri1 = string.Format("bingmaps:?rtp=adr.{0}~adr.{1}", currentAddress, Uri.EscapeDataString(SelectedProperty.FullAddress));
                await Windows.System.Launcher.LaunchUriAsync(new Uri(uri1));
            }
            catch (Exception exc)
            {
                if ((uint)exc.HResult == 0x80004004)
                {
                    // the application does not have the right capability or the location master switch is off
                    DialogUtility.ShowMessage(DialogMessagesErrorsAndConstants.LocationSwitchedOffSmall, DialogMessagesErrorsAndConstants.LocationSwitchedOff);
                }
                else
                {
                    DialogUtility.ShowMessage(DialogMessagesErrorsAndConstants.ErrorOccured);
                }
            }
            HidePopupAndEnable();
        }

        private void HidePopupAndEnable()
        {
            DirectionsEnabled = true;
            PopupIsOpen = false;
            PopupMessageVisible = Visibility.Collapsed;
        }

        private void ShowPopupAndDisable()
        {
            DirectionsEnabled = false;
            PopupIsOpen = true;
            PopupMessageVisible = Visibility.Visible;
        }

        public void ShowDirections()
        {
            ActionsVisible = Visibility.Collapsed;
            DirectionsVisible = Visibility.Visible;
        }

        public void HideDirections()
        {
            ActionsVisible = Visibility.Visible;
            DirectionsVisible = Visibility.Collapsed;
        }

        private async void PropertyFavouritedValidation()
        {
            if (NetworkInformation.GetInternetConnectionProfile() != null)
            {
                try
                {
                    List<FavouriteProperty> properties =
                        await App.FavouritePropertiesTable.Where(property => property.UserId == ApplicationResourcesUtility.GetLoggedOnUser().Id &&
                            property.PropertyId == SelectedProperty.Id).Take(1).ToListAsync();
                    if (properties.Count > 0)
                    {
                        IsChecked = true;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                    DialogUtility.ShowMessage(DialogMessagesErrorsAndConstants.ErrorOccured);
                }
            }
            else
            {
                DialogUtility.NoNetworkConnection();
            }
        }
    }
}
