﻿using Windows.Devices.Geolocation;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Controls.Maps;
using HomeKeeper.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HomeKeeper.ViewModels;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace HomeKeeper.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PropertyDetailView : Page
    {
        private NavigationHelper navigationHelper;
        private PropertyDetailsViewModel _propertyDetailsViewModel;

        public PropertyDetailView()
        {
            this.InitializeComponent();
            _propertyDetailsViewModel = new PropertyDetailsViewModel();
            DataContext = _propertyDetailsViewModel;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            SetupMap(_propertyDetailsViewModel.SelectedProperty.Latitude,
                _propertyDetailsViewModel.SelectedProperty.Longitude);
        }

        //Method for setting up the map and zooming in on the location of the selected property
        private void SetupMap(string latitude, string longitude)
        {
            //Creating a new BasicGeoposition using our passed-in parameters
            BasicGeoposition position = new BasicGeoposition
            {
                Latitude = Convert.ToDouble(latitude),
                Longitude = Convert.ToDouble(longitude)
            };

            //Constructing a new Geopoint object and passing in the position from above
            Geopoint geopoint = new Geopoint(position);
            propertyMap.Center = geopoint;
            propertyMap.ZoomLevel = 15;


            //Creating map pin using mapIcon
            MapIcon MapIcon1 = new MapIcon
            {
                Location = new Geopoint(position),
                NormalizedAnchorPoint = new Point(1.0, 0.5),
                Title = _propertyDetailsViewModel.SelectedProperty.AddressLine1,
                Visible = true,
                ZIndex = int.MaxValue,
                Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/MapIcon.png"))
            };

            //Adding pin to map
            propertyMap.MapElements.Add(MapIcon1);
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void PropertyDetailsPivot_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Pivot pivot = sender as Pivot;

            if (pivot != null && _propertyDetailsViewModel != null)
            {
                if (pivot.SelectedIndex == 1)
                {
                    _propertyDetailsViewModel.ShowDirections();
                }
                else
                {
                    _propertyDetailsViewModel.HideDirections();
                }
            }
        }
    }
}
