﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using HomeKeeper.Models;
using HomeKeeper.Utilities;
using Windows.UI.Xaml;
using HomeKeeper.Utilities.Constants;
using Newtonsoft.Json.Linq;

namespace HomeKeeper.ViewModels
{
    public class FavouritePropertiesViewModel : ViewModelBase
    {
        private List<Property> _favouriteProperties = new List<Property>();

        public List<Property> FavouriteProperties
        {
            get { return _favouriteProperties; }
            set
            {
                if (_favouriteProperties != value)
                {
                    _favouriteProperties = value;
                    RaisePropertyChanged();
                }
            }
        }


        private Visibility _propertiesVisible;

        public Visibility PropertiesVisible
        {
            get { return _propertiesVisible; }
            set
            {
                if (_propertiesVisible != value)
                {
                    _propertiesVisible = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Visibility _emptyVisible;

        public Visibility EmptyVisible
        {
            get { return _emptyVisible; }
            set
            {
                if (_emptyVisible != value)
                {
                    _emptyVisible = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Property _selectedProperty;

        public Property SelectedProperty
        {
            get { return _selectedProperty; }
            set
            {
                if (_selectedProperty != value)
                {
                    _selectedProperty = value;
                    ApplicationResourcesUtility.SetSelectedProperty(_selectedProperty);
                    NavigationUtility.NavigateToPropertyDetails();
                    RaisePropertyChanged();
                }
            }
        }

        private string _fetchEmptyText = DialogMessagesErrorsAndConstants.FetchingFavouriteProperties;

        public string FetchEmptyText
        {
            get { return _fetchEmptyText; }
            set
            {
                if (_fetchEmptyText != value)
                {
                    _fetchEmptyText = value;
                    RaisePropertyChanged();
                }
            }
        }

        public RelayCommand HomeCommand { get; set; }
        public RelayCommand ProfileCommand { get; set; }
        public RelayCommand LogoutCommand { get; set; }

        public FavouritePropertiesViewModel()
        {
            SetupNavigation();
            GetFavouritedProperties();
        }

        public void SetupNavigation()
        {
            HomeCommand = new RelayCommand(NavigationUtility.NavigateToHome);
            ProfileCommand = new RelayCommand(NavigationUtility.NavigateToProfileUpdateDetails);
            LogoutCommand = new RelayCommand(NavigationUtility.NavigateToLogin);
        }

        public async void GetFavouritedProperties()
        {
            if (NetworkInformation.GetInternetConnectionProfile() != null)
            {
                try
                {
                    User user = ApplicationResourcesUtility.GetLoggedOnUser();

                    JArray result = await App.MobileService
                                    .InvokeApiAsync<JArray>(DialogMessagesErrorsAndConstants.FavouritePropertyController,
                                        System.Net.Http.HttpMethod.Get, new Dictionary<string, string>
                                                    {
                                                        {"userId", user.Id}
                                                    });

                    FavouriteProperties = result.ToObject<List<Property>>().ToList();
                    
                    if (FavouriteProperties.Count > 0)
                    {
                         EmptyVisible = Visibility.Collapsed;
                    }
                    else
                    {
                        EmptyVisible = Visibility.Visible;
                        FetchEmptyText = DialogMessagesErrorsAndConstants.NoFavouritePropertiesFound;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                    DialogUtility.ShowMessage(DialogMessagesErrorsAndConstants.ErrorOccured);
                }
            }
            else
            {
                DialogUtility.NoNetworkConnection();
            }    
        }
    }
}
