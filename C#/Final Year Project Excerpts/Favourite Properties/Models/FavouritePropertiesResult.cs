﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HomeKeeper.Models
{
    public class FavouritePropertiesResult
    {
        public string Id { get; set; }
        public string PropertyId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string CityTown { get; set; }
        public string Postcode { get; set; }
        public double Deposit { get; set; }
        public double MonthlyRent { get; set; }
        public bool IncludesRates { get; set; }
        public DateTime ViewableFrom { get; set; }
        public DateTime AvailableFrom { get; set; }
        public int NumBedrooms { get; set; }
        public int NumBathrooms { get; set; }
        public string Heating { get; set; }
        public string RentalStatus { get; set; }
        public bool Furnished { get; set; }
        public string AdditionalDescription { get; set; }
        public string ManagerId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string NumViewings { get; set; }
        public string PrimaryPicture { get; set; }
    }
}
