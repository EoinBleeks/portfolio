﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;

namespace HomeKeeper.Models
{
    public class FavouriteProperty
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "userId")]
        public string UserId { get; set; }

        [JsonProperty(PropertyName = "propertyId")]
        public string PropertyId { get; set; }

        public bool Deleted { get; set; }
    }
}
