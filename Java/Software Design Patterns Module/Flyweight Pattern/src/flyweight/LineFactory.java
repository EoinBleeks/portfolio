package flyweight;

import java.awt.Color;
import java.util.HashMap;

public class LineFactory {
	private HashMap<Color, Line> linePool;
	
	public LineFactory(){
		linePool = new HashMap<Color, Line>();
	}
		
	public Line getLine(Color colour){
		Line line = linePool.get(colour);
		if(line == null){
			line = new Line(colour);
			linePool.put(colour, line);
			System.out.println("Creating "+colour+" line");
		}
		return line;
	}

}
