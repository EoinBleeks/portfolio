package command;

public class DriveCommand implements Command {
	private Robot robot;
	
	public DriveCommand(Robot robot){
		this.robot = robot;
	}
	
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		this.robot.drive();
	}

	@Override
	public void setRobotModel(Robot model) {
		// TODO Auto-generated method stub
		this.robot = model;
	}

}
