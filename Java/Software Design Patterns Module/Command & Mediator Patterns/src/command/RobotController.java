package command;

public class RobotController {
	public Command driveCommand, grabCommand;

	public void setCommands(Command driveCommand, Command grabCommand) {
		this.driveCommand = driveCommand;
		this.grabCommand = grabCommand;
	}

	public void fireCommand(Command command) {
		command.execute();
	}

}
