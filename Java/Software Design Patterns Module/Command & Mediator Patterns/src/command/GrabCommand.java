package command;

public class GrabCommand implements Command {
	private Robot robot;
	
	public GrabCommand(Robot robot){
		this.robot = robot;
	}
	
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		this.robot.grab();
	}

	@Override
	public void setRobotModel(Robot model) {
		// TODO Auto-generated method stub
		this.robot = model;
	}

}
