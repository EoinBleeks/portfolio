package mediator;
interface Command {
    void execute();
}
