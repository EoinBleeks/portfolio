package mediator;

import mediator.BtnEdit;

class Mediator {
	private BtnEdit btnEdit;
	private BtnBrowse btnBrowse;
	private BtnSearch btnSearch;
	private ChkLock chkLock;
	private LblDisplay lblFeedback;

    public void registerEditButton(BtnEdit edt) {
        btnEdit = edt;
    }
    
    public void registerBrowseButton(BtnBrowse browse){
    	btnBrowse = browse;
    }
    
    public void registerSearchButton(BtnSearch search){
    	btnSearch = search;
    }
    
    public void registerLockCheckBox(ChkLock lock){
    	chkLock = lock;
    }
    
    public void registerFeedbackLabel(LblDisplay display){
    	lblFeedback = display;
    }
 
    public void browseClicked() {
        btnBrowse.setEnabled(false);
        btnSearch.setEnabled(true);
        lblFeedback.setText("You are now browsing.");
        if (chkLock.isSelected())
        	btnEdit.setEnabled(true);
    }
    
    public void searchClicked(){
    	btnBrowse.setEnabled(true);
    	btnSearch.setEnabled(false);
    	lblFeedback.setText("You are now searching.");
    	if(chkLock.isSelected()){
    		btnEdit.setEnabled(true);
    	}
    }
    
    public void editClicked(){
    	btnBrowse.setEnabled(false);
    	btnSearch.setEnabled(true);
    	lblFeedback.setText("You are now editing.");
    	if(chkLock.isSelected()){
    		btnEdit.setEnabled(true);
    	}
    }

    public void lockSelected(){
    	if(chkLock.isSelected()){
    		btnEdit.setEnabled(false);
    	}
    	
    }
    
    public void unlockUnselected(){
    	if(!chkLock.isSelected()){
    		btnEdit.setEnabled(true);
    	}
    }

}
