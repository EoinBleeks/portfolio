package builder;

public interface WriteUpBuilder {
	
	public void printTitle();
	public void printTableOfContents();
	public void printSummary();
	public void printChapter();
	public void printBibliography();
	
}
