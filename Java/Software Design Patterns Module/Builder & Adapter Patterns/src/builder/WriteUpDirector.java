package builder;

public class WriteUpDirector {
	private WriteUpBuilder builder = null;
	
	public WriteUpDirector(WriteUpBuilder builder){
		this.builder = builder;
		System.out.println("Director Created");
	}
	
	public void constructSchedule(){
		builder.printTitle();
		builder.printTableOfContents();
		builder.printSummary();
		builder.printChapter();
		builder.printBibliography();
	}
}
