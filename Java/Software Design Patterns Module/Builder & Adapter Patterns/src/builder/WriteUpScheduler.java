package builder;

import java.nio.channels.WritePendingException;

public class WriteUpScheduler {

	public static void main(String[] args) {
		System.out.println("Printing project write up");
		WriteUpBuilder builder = new DissertationWriteUpBuilder();
		WriteUpDirector director = new WriteUpDirector(builder);
		director.constructSchedule();
		System.out.println("Project Printed");
		System.out.println();
		System.out.println();
		System.out.println("Printing PhD Thesis");
		builder = new ThesisWriteUpBuilder();
		director = new WriteUpDirector(builder);
		director.constructSchedule();
		System.out.println("PhD Thesis printed");
		
	}
}
