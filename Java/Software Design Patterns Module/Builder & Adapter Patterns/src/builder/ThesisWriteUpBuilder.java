package builder;

public class ThesisWriteUpBuilder implements WriteUpBuilder {
	
	public ThesisWriteUpBuilder() {
		System.out.println("Builder for Thesis Created");
	}

	@Override
	public void printTitle() {
		// TODO Auto-generated method stub
		System.out.println("printing project title");
	}

	@Override
	public void printTableOfContents() {
		// TODO Auto-generated method stub
		System.out.println("printing PhD table of contents");
	}

	@Override
	public void printSummary() {
		// TODO Auto-generated method stub
		System.out.println("printing PhD abstract");
	}

	@Override
	public void printChapter() {
		// TODO Auto-generated method stub
		System.out.println("printing PhD Thesis chapters");
	}

	@Override
	public void printBibliography() {
		// TODO Auto-generated method stub
		System.out.println("printing PhD References");
	}
	
}
