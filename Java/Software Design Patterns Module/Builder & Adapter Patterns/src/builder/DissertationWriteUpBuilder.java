package builder;

public class DissertationWriteUpBuilder implements WriteUpBuilder {
	
	public DissertationWriteUpBuilder() {
		// TODO Auto-generated constructor stub
		System.out.println("Builder for Dissertation created");
	}

	@Override
	public void printTitle() {
		// TODO Auto-generated method stub
		System.out.println("printing project title");
	}

	@Override
	public void printTableOfContents() {
		// TODO Auto-generated method stub
		System.out.println("printing project table of contents");
	}

	@Override
	public void printSummary() {
		// TODO Auto-generated method stub
		System.out.println("printing project summary");
	}

	@Override
	public void printChapter() {
		// TODO Auto-generated method stub
		System.out.println("printing project chapters");
	}

	@Override
	public void printBibliography() {
		// TODO Auto-generated method stub
		System.out.println("printing project biblography");
	}
}