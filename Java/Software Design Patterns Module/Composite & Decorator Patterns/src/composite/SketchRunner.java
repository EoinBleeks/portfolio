package composite;

public class SketchRunner {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SketchedItem house = new Sketch("House", 3);
		SketchedItem path = new Sketch("Path", 6);
		SketchedItem car = new Sketch("Car", 5);

		SketchedItem picture = new Drawing("My Picture", 0);
		//add items to the drawing here
		//picture.addItem(house);
		picture.addItem(path);
		picture.addItem(car);
		//print out single items here
		SketchedItem [] sketchList = picture.getSketchedItems();
		
		for(SketchedItem item : sketchList){
			System.out.println(item.getDescription());
		}
		
		//print out a drawing (composite) here	
		SketchedItem composite = new Drawing("composite", 0);
		composite.addItem(picture);
		System.out.println();
		//System.out.println(house.toString());
		System.out.println(path.toString());
		System.out.println(car.toString());
		System.out.println();
		System.out.println(picture.toString());
		System.out.println();
		System.out.println(composite.toString());
		System.out.println();
		System.out.println("My Picture consisting of "+path.toString()+ " "+car.toString()+" ");
		}
}

