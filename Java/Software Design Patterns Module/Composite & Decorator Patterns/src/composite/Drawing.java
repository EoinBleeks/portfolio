package composite;

import java.util.ArrayList;
import java.util.List;

public class Drawing extends SketchedItem {
	private List<SketchedItem> items;

	public Drawing(String description, int cost) {
		super(description, cost);
		// TODO Auto-generated constructor stub
		items = new ArrayList<SketchedItem>();
	}

	@Override
	public void addItem(SketchedItem item) {
		// TODO Auto-generated method stub
		items.add(item);
	}

	@Override
	public void removeItem(SketchedItem item) {
		// TODO Auto-generated method stub
		items.remove(item);
	}

	@Override
	public SketchedItem[] getSketchedItems() {
		// TODO Auto-generated method stub
		return items.toArray((new SketchedItem[items.size()]));
	}
	
	public int getCost(){
		int total = 0;
		for(SketchedItem item : items){
			total += item.getCost();
		}
		return total;
	}

}
