package decorator;

public class DecoratedCharacterTest {

    public static void main(String[] args) {
    	// 1. demo of a simple window
    	System.out.println("1. Test Simple Character");
    	// create a simple character and print out its description;    	
    	Character simple = new SimpleCharacter();
    	//simple.draw();
    	System.out.println(simple.getDescription());
    	//simple.getDescription();
    	System.out.println();
    	
    	// 2. demo of a character decorated with a hat
    	System.out.println("2. Test Character with hat");
    	Character simpleHat = new SimpleCharacter();
    	simpleHat = new HatDecorator(simpleHat);
    	// create a character decorated with a hat and print out its description;   
    	//simpleHat = new HatDecorator(simpleHat);
    	simpleHat.draw();
    	System.out.println(simpleHat.getDescription());
    	System.out.println();
        
    	// 3. demo of  a character decorated with a coat
    	System.out.println("3. Test Character with coat");
    	// create a character decorated with a hat and print out its description; 
    	Character simpleCoat = new SimpleCharacter();
        simpleCoat = new CoatDecorator(simpleCoat);
        simpleCoat.draw();
        System.out.println(simpleCoat.getDescription());
        System.out.println();
        
    	// 4. demo of a character decorated with a hat and coat
        System.out.println("4. Test Character with hat and coat");
     // create a character decorated with a hat AND coat and print out its description; 
        Character hatAndCoat = new SimpleCharacter();
        hatAndCoat = new CoatDecorator(hatAndCoat);
        hatAndCoat.draw();
        hatAndCoat = new HatDecorator(hatAndCoat);
        hatAndCoat.draw();
        System.out.println(hatAndCoat.getDescription());

    }
}