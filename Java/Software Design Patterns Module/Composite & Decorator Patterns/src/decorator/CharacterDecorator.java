package decorator;

public abstract class CharacterDecorator extends SimpleCharacter {
	protected Character decoratedCharacter;
	
	public CharacterDecorator(Character character){
		super();
		decoratedCharacter = character;
	}
	
}
