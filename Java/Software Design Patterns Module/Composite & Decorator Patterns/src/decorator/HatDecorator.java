package decorator;

public class HatDecorator extends CharacterDecorator{

	public HatDecorator(Character character) {
		super(character);
	}
	
	public void draw() {
		System.out.println("... Drawing a Hat on Character");
		// draw functionality
	}

	public String getDescription() {
		return decoratedCharacter.getDescription() + ", decorated with a hat";
		// return the description
	}
	
}
