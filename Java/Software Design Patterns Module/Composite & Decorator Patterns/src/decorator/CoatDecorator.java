package decorator;

public class CoatDecorator extends CharacterDecorator {
	
	public CoatDecorator(Character character) {
		super(character);
	}
	
	public void draw() {
		System.out.println("... Drawing a coat on Character");
		// draw functionality
	}

	public String getDescription() {
		return decoratedCharacter.getDescription() + ", decorated with a coat";
		// return the description
	}

}
