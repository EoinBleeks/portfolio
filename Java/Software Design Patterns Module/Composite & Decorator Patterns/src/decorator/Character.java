package decorator;

public interface Character {
    public void draw(); // draws the Character 
    public String getDescription(); // returns a description of the Character
}
