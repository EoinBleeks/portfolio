/*===========================================================
* (C) Hans Vandierendonck, 2013-14
 *===========================================================*/
package q1;
import java.util.concurrent.*;
import java.util.Random;
import java.util.HashSet;

/*===========================================================
 * Auxiliary class to perform timing measurement and control
 * the experiments.
 *===========================================================*/
class ResultAggregator  {
    private int num_values;
    private int num_rounds;
    private int round;
    private final CyclicBarrier barrier;
    private long sum_rate;
    private long sum_rate_sq;
    private double sum_delay;
    private long last_time;
    private Contents contents;

    ResultAggregator( int nv, int nr, int np, Contents c ) {
	num_values = nv;
	num_rounds = nr;
	round = 0;
	sum_rate = sum_rate_sq = 0;
	sum_delay = 0;
	last_time = System.nanoTime();
	contents = c;

	barrier = new CyclicBarrier( np,
				     new Runnable() {
					 public void run() {
					     long now = System.nanoTime();
					     long delay = now - last_time;
					     double f_delay = (double)delay * 1e-9;
					     if( round > 0 ) { // skip 1st round
						 double r = (double)num_values / f_delay;
						 sum_rate += r;
						 sum_rate_sq += r*r;
						 sum_delay += f_delay;
					     }
					     round++;
					     if( round > 1 ) {
						 System.out.println( "Delay is " + f_delay + " secs" );
					     } else {
						 System.out.println( "delay is " + f_delay + " secs (warmup)" );
					     }
					     contents.nextRound();
					     System.gc();
					     System.gc();
					     System.gc();
					     last_time = System.nanoTime();
					 }
				     } );
    }

    public boolean isFinished() {
	return round >= num_rounds;
    }

    public double getAvgDelay() {
	return (double)sum_delay / (double)(num_rounds-1);
    }

    public double getAvgRate() {
	return (double)sum_rate / (double)(num_rounds-1);
    }

    public double getStdDevRate() {
	double s1 = (double)sum_rate;
	double s2 = (double)sum_rate_sq;
	double N = (double)(num_rounds-1);
	return Math.sqrt( (N*s2-s1*s1)/(N*(N-1)) );
    }

    public void syncBarrier() {
	try {
	    barrier.await();
	} catch( InterruptedException ex ) {
	} catch( BrokenBarrierException ex ) {
	}
    }
}

/*===========================================================
 * Auxiliary class to hold a value
 *===========================================================*/
class IntValue {
    int v;

    IntValue( int vv ) { v = vv; }

    @Override
    public int hashCode() {
	return v;
    }

    @Override
    public boolean equals( Object obj ) {
	if( !(obj instanceof IntValue) )
	    return false;
	if( obj == this )
	    return true;

	IntValue rhs = (IntValue)obj;
	return v == rhs.v;
    }
}

/*===========================================================
 * Contents generation
 *===========================================================*/
class Contents {
    private int num_values;
    private int num_elems;
    private int nthreads;
    private int num_draws;
    private int elems_per_thread;
    private int values[];
    private int sequence[];
    private Hash<IntValue,IntValue> hashtable;
    private final Random rng = new Random();
    public static final int MAX_RANDOM_VALUE = 1<<30;

    Contents( int nv, int ne, int nt, int nd ) {
	num_values = nv;
	num_elems = ne;
	nthreads = nt;
	num_draws = nd;
	elems_per_thread = num_elems / nthreads;
	values = new int[num_elems];
	sequence = new int[num_draws];
    }

    void nextRound() {
	HashSet<IntValue> set = new HashSet<IntValue>();
	for( int i=0; i < num_elems; ++i )
	    set.add( new IntValue(values[i]) );

	for( int i=0; i < num_draws; ++i ) {
	    do {
		sequence[i] = rng.nextInt(MAX_RANDOM_VALUE)*64;
	    } while( set.contains( new IntValue(sequence[i]) ) );
	    set.add( new IntValue(sequence[i]) );
	}
    }

    void initialize( Hash<IntValue,IntValue> hash ) {
	hashtable = hash;
	for( int i=0; i < num_elems; ++i ) {
	    do {
		values[i] = rng.nextInt(MAX_RANDOM_VALUE)*64;
	    } while( !hashtable.add( new IntValue(values[i]), new IntValue(1) ) );
	}

	nextRound();
    }

    int getsl( int k ) {
	// A successful lookup
	return values[k];
    }
    int getad( int no, int tid ) {
	return sequence[tid*num_values+no]; // replacement
    }
    int getrm( int k, int no, int tid ) {
	// A successful lookup
	int pos = (k %  elems_per_thread) + tid * elems_per_thread;
	// System.out.println( "pos=" + pos + " k=" + k + " tid=" + tid );
	int ret = values[pos];
	values[pos] = sequence[tid*num_values+no]; // replacement
	return ret;
    }
};

/*===========================================================
 * Process definition for processes that will generate
 * accesses to the hash table.
 *===========================================================*/
class TestProcess extends Thread {
    private Hash<IntValue,IntValue> hashtable;
    private int num_values;
    private int num_elems;
    private int tid;
    private ResultAggregator agg;
    private Contents contents;

    TestProcess( int num_values_, int num_elems_, int tid_,
		 Hash<IntValue,IntValue> hash, ResultAggregator agg_,
		 Contents cnt_ ) {
	hashtable = hash;
	num_values = num_values_;
	num_elems = num_elems_;
	tid = tid_;
	agg = agg_;
	contents = cnt_;
    }

    public void run() {
	final Random rng = new Random();
	while( !agg.isFinished() ) {
	    for( int v=0; v < num_values; ++v ) {
		// An unsuccessful lookup
		int k = rng.nextInt(Contents.MAX_RANDOM_VALUE);
		hashtable.get( new IntValue(k*64+63) );

		int kg = rng.nextInt(num_elems);
		int kr = rng.nextInt(num_elems);
		int vg = contents.getsl( kg );
		int va = contents.getad( v, tid );
		int vr = contents.getrm( kr, v, tid );

		// System.out.println( "lookup: " + vg + " remove: " + vr + " add:" + va + " tid=" + tid );

		// A successful lookup
		hashtable.get( new IntValue(vg) );

		// A random deletion
		if( !hashtable.remove( new IntValue(vr) ) ) {
		    // System.out.println( "Remove failed: " + vr + " tid=" + tid );
		} else {
		    // System.out.println( "Remove ok: " + vr + " tid=" + tid );
		}

		// A random insertion
		hashtable.add( new IntValue(va), new IntValue(1) );
	    }
	    agg.syncBarrier();
	}
    }
}

/*===========================================================
 * Main Driver Class (main program)
 *===========================================================*/
class Driver {
    public static int parse_integer( String arg, int pos ) {
	int i = 0;
	try {
	    i = Integer.parseInt( arg );
	} catch( NumberFormatException e ) {
	    System.err.println( "Argument " + pos + "'" + arg
				+ "' must be an integer" );
	}
	return i;
    }

    public static double parse_double( String arg, int pos ) {
	double i = 0;
	try {
	    i = Double.parseDouble( arg );
	} catch( NumberFormatException e ) {
	    System.err.println( "Argument " + pos + "'" + arg
				+ "' must be a double" );
	}
	return i;
    }

    public static void main (String[] args) {
	if( args.length < 4 ) {
	    System.err.println("Usage: Driver <num_threads> <init-size> <measure-elms> <rounds>");
	    System.exit( 1 );
	}

	int nthreads = parse_integer( args[0], 1 );
	int init_size = parse_integer( args[1], 2 );
	int num_measured = parse_integer( args[2], 3 );
	int num_rounds = parse_integer( args[3], 4 );

	if( nthreads > init_size ) {
	    // Need this. The reason is that contents holds an unsynchronized
	    // shared array (if we synchronize it we loose concurrency) and
	    // each thread is given a slice in this array. Calculation of the
	    // address in the slice depends on this property.
	    System.err.println( "Hashtable must have at least as many elements as there are threads." );
	    System.exit( 1 );
	}

        System.out.println("Measuring performance with " + nthreads
			   + " threads."
			   + " Doing " + num_rounds
			   + " rounds of the experiment after a warmup"
			   + " round and " + num_measured
			   + " put and get operations per round.");

	Contents contents = new Contents( num_measured, init_size, nthreads,
					  num_measured*nthreads );
	ResultAggregator agg
	    = new ResultAggregator( num_measured*nthreads, num_rounds, nthreads,
				    contents );

	TestProcess[] processes = new TestProcess[nthreads];

	Hash<IntValue,IntValue> hashtable = new Hash<IntValue,IntValue>();

	// Initialize the hash table with values to start non-empty
	contents.initialize( hashtable );

	// Create all of the threads
	for( int i=0; i < nthreads; ++i ) {
	    processes[i] = new TestProcess( num_measured, init_size, i,
					    hashtable, agg, contents );
	}

	// Start all of the threads and let them warmup.
	// Warming up is important to let the JIT do it's work (i.e.,
	// compile and optimize the code to make ti faster). When the JIT
	// kicks in, measured performance numbers are distorted and unreliable.
	// The garbage collector is another source of disruption to performance.
	// That's why will call the GC explicitly when reaching the barrier
	// (see barrier creation). You would normally never call the GC
	// directly.
	for( int i=0; i < nthreads; ++i ) {
	    processes[i].start();
	}

	// Join threads (cleanup properly).
	for( int i=0; i < nthreads; ++i ) {
	    try { processes[i].join(); } catch( InterruptedException e ) { }
	}

	System.out.println( "Hashtable size is: " + hashtable.size() );

	// Get the results out.
	double avg_delay = agg.getAvgDelay();
	double avg_rate = agg.getAvgRate();
	double sdv_rate = agg.getStdDevRate();

	System.out.println( "Average delay of a trial was " + avg_delay
			    + " seconds." );
	System.out.println( "Average operations/sec: " + avg_rate );
	System.out.println( "Standard deviation operations/sec: " + sdv_rate );
    }
}
