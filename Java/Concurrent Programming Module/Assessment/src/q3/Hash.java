package q3;
public class Hash<K,V> {
    private static final int BUCKETS = 15;
    private Chain<K,V>[] buckets;

    public Hash() {
	buckets = (Chain<K,V>[]) new Chain[BUCKETS];
	for( int i=0; i < BUCKETS; ++i )
	    buckets[i] = new Chain<K,V>();
    }

    private int bHash( int hash ) {
	return Math.abs(hash % BUCKETS);
    }

    public boolean add( K key, V value ) {
	int bhash = bHash( key.hashCode() );
	return buckets[bhash].add( key, value );
    }

    public V get( K key ) {
	int bhash = bHash( key.hashCode() );
	return buckets[bhash].get( key );
    }

    public boolean remove( K key ) {
	int bhash = bHash( key.hashCode() );
	return buckets[bhash].remove( key );
    }

    public synchronized int size() {
	int size = 0;
	for( int i=0; i < BUCKETS; ++i )
	    size += buckets[i].size();
	return size;
    }
}
